# Reasoning #

***

In Poland according to Polish Constitution - receipts, agreements, documentation etc regarding expenses which were incurred from public funds need to be available to polish residents upon request. Unfortunately many politicians are resistant to this law. During election taking place in Poland in 2014 [NGO Watchdog Poland][siec-obywatelska] ordered the web application, which allows to send to future councillors, presidents, and politicians petition. The petition asked them to make an obligation, that if they get elected, they will not break any transparency laws, and that they will publish all public expenses incurred during their run on Internet.

## Petition ##

This web application keeps track of inquiries sent to any politician:

* sent petition to any politician by wide audience,
* track response from politicians,

## Technology Used ##

* Backend - Django
* Frontend - Angular 1.x

[siec-obywatelska]: http://siecobywatelska.pl/?lang=en