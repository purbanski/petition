# -*- coding: utf-8 -*-
from django.contrib import admin
from petycja.models import Nadawca
from petycja.models import Organizacja
from petycja.models import Odbiorca
from petycja.models import Wniosek
from petycja.models import Miasto
from petycja.models import Wojewodztwo


class OrganizacjaInline(admin.StackedInline):
    model = Odbiorca
    extra = 3
    
class ChoiceInline(admin.StackedInline):
    model = Organizacja
    extra = 3
    
class OdbiorcaAdmin(admin.ModelAdmin):
#     fields = ['imie', 'nazwisko','email']
    list_display = ('imie', 'nazwisko', 'druga_tura', 'email','miasto', 'orgranizacja', 'plec', 'wniosek_status')
    list_filter = ('plec', 'druga_tura', 'wniosek_status', )
    fieldsets = [
                 ('Osoba',       {'fields': ['imie', 'nazwisko', 'plec', 'email', 'miasto', 'orgranizacja', 'wniosek_status', 'facebook', 'twitter', 'druga_tura']}),
#                  ('Nazwisko',   {'fields': ['nazwisko']}),
#                  ('Nazwisko',   {'fields': ['nazwisko'], 'classes': ['collapse']}),
    ]   
#     inlines = [ChoiceInline]
    
class OrganizacjaAdmin(admin.ModelAdmin):
    fields = ['skrot','nazwa']
    list_display = ('skrot','nazwa')

class WojewodztwoAdmin(admin.ModelAdmin):
    fields = ['nazwa']
    list_display = ['nazwa']

class MiastoAdmin(admin.ModelAdmin):
    fields = ['miasto', 'wojewodztwo']
    list_display = ('miasto', 'wojewodztwo')

class NadawcaAdmin(admin.ModelAdmin):
    fields = ['imie','nazwisko','email']
    list_display = ('imie','nazwisko','email')

class WniosekAdmin(admin.ModelAdmin):
    fields = ['data','nadawca','odbiorca']
    list_display = ('data','nadawca','odbiorca')

admin.site.register(Wniosek, WniosekAdmin)    
admin.site.register(Odbiorca, OdbiorcaAdmin)
admin.site.register(Organizacja, OrganizacjaAdmin)
admin.site.register(Nadawca, NadawcaAdmin)
admin.site.register(Miasto, MiastoAdmin)
admin.site.register(Wojewodztwo, WojewodztwoAdmin)
