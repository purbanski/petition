from django.db import models
from datetime import datetime, timedelta

def default_start_time():
    now = datetime.now()
    start = now.replace(hour=22, minute=0, second=0, microsecond=0)
    return start if start > now else start + timedelta(days=1)  

# Create your models here.
class Organizacja(models.Model):
    skrot = models.CharField(max_length=16)
    nazwa = models.CharField(unique=True,max_length=64)

    def __unicode__(self):
        return "%s" % (self.nazwa)
    
    def as_dict(self):
        return {
            'id': self.id,
            'skrot': self.skrot,
            'nazwa': self.nazwa
        }

class Wojewodztwo(models.Model):
    nazwa    = models.CharField(unique=True,max_length=64)
    
    def __unicode__(self):
        return "%s" % (self.nazwa)

    def as_dict(self):
        return {
            'id': self.id,
            'nazwa': self.nazwa,
        }

class Miasto(models.Model):
    miasto      = models.CharField(max_length=64)
    wojewodztwo = models.ForeignKey(Wojewodztwo)
   
    def __unicode__(self):
#         return "%s" % (self.miasto)
        return "%s / %s " % (self.miasto, self.wojewodztwo.nazwa)

    def as_dict(self):
        return {
            'id': self.id,
            'miasto': (self.miasto),
            'id_wojewodztwo': (self.wojewodztwo.id),
            'wojewodztwo': (self.wojewodztwo.nazwa)
        }

class Odbiorca(models.Model):
    WNIOSEK_STATUS = (
        ('X', 'Pusty'),
        ('W', 'Wyslany'),
        ('P', 'Podpisany'),
        ('O', 'Odrzucony')
    )
    
    PLEC = (
        ('M','Mezczyzna'),
        ('K','Kobieta')
    )
    
    imie     = models.CharField(max_length=64)
    nazwisko = models.CharField(max_length=64)
    plec     = models.CharField(max_length=1, default='M', choices=PLEC)
    email    = models.EmailField(max_length=64)
    twitter  = models.CharField(max_length=64,default="BRAK")
    facebook = models.CharField(max_length=64,default="BRAK")
    druga_tura  = models.BooleanField(default=False,db_column="tura",help_text="Zaznacz je&#347;li kandydat dosta&#322; si&#281; do drugiej tury")
    
#     orgranizacja    = models.ManyToOneField(Organizacja)
    orgranizacja    = models.ForeignKey(Organizacja)
#     wojewodztwo     = models.ForeignKey(Wojewodztwo)
#     miasto          = models.OneToOneField(Miasto)
    miasto          = models.ForeignKey(Miasto)
    
    wniosek_status  = models.CharField(max_length=1, default='X', choices=WNIOSEK_STATUS)

    def __unicode__(self):
        return "%s %s (%s)" % (self.imie, self.nazwisko, self.orgranizacja.skrot)

    def as_dict(self):
        return {
            'id': self.id,
            'imie': self.imie,
            'plec': self.plec,
            'nazwisko': self.nazwisko,
#             'email': self.email,
            'organizacja': self.orgranizacja.nazwa,
    
            'id_miasto'  : self.miasto.id,
            'miasto'     : self.miasto.miasto,
            'wojewodztwo': self.miasto.wojewodztwo.nazwa,
            'twitter'   : self.twitter,
            'facebook'  : self.facebook,
            'wniosek_status' : self.wniosek_status
            # other stuff
        }

class Nadawca(models.Model):
    imie     = models.CharField(max_length=64)
    nazwisko = models.CharField(max_length=64)
    email    = models.EmailField(max_length=64)
    adres    = models.CharField(max_length=256)
    
    def __unicode__(self):
        return "%s %s (%s)" % (self.imie, self.nazwisko, self.email)

class Wniosek(models.Model):
#     WNIOSEK_WYSLANY = (
#         ('X', 'Brak danych'),
#         ('S', 'Wysylanie'),
#         ('W', 'Wyslany'),
#         ('P', 'Problem'))
    
    data        = models.DateTimeField(default=default_start_time)
    nadawca     = models.ForeignKey(Nadawca)
    odbiorca    = models.ForeignKey(Odbiorca)
#     wniosek_wyslany = models.CharField(max_length=1, default='X', choices=WNIOSEK_WYSLANY)
   
    def __unicode__(self):
        return "%s %s" % (self.data, self.nadawca)

