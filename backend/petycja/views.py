# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.http import HttpResponse

from petycja.models import Odbiorca, Wniosek, Nadawca, Organizacja, Miasto, Wojewodztwo
from petycja.wniosek import get_petycja_text

from django.core.mail import send_mail
from django.core.mail import EmailMessage   
from django.db.models import Count, Sum, F

from config import Config

import json
import itertools
import pprint

# import pdb 
# pdb.set_trace()

## -----------------------------------------------------
def str_to_json(callback,recs):
#     dict = [ obj.as_dict() for obj in recs ]
    tmp = []
    tmp.append(recs)
    recs = tmp
    data = json.dumps({'data': recs})
    ret = callback+'('+data+')'
    
    return HttpResponse(ret, content_type='application/json')

## -----------------------------------------------------
def query_to_json(callback,recs):
    dict = [ obj.as_dict() for obj in recs ]
    data = json.dumps({'data': dict})
    ret = callback+'('+data+')'
    
    return HttpResponse(ret, content_type='application/json')

## -----------------------------------------------------
def odbiorcy(request, callback):
    recs = Odbiorca.objects.all().order_by('miasto','imie','nazwisko') 
    return query_to_json(callback,recs)

## -----------------------------------------------------
def odbiorcy_druga_tura(request, callback):
    recs = Odbiorca.objects.filter(druga_tura=True).all().order_by('miasto','imie','nazwisko') 
    return query_to_json(callback,recs)
    
## -----------------------------------------------------
def organizacje(request, callback):
    recs = Organizacja.objects.all()
    return query_to_json(callback,recs)

## -----------------------------------------------------
def wojewodztwa(request, callback):
    recs = Wojewodztwo.objects.all().order_by('nazwa')
    return query_to_json(callback,recs)

## -----------------------------------------------------
def miasta(request, callback):
    recs = Miasto.objects.all().order_by('miasto')
    return query_to_json(callback,recs)

## -----------------------------------------------------
def petycja_stats(request, callback):
    wyslane = Wniosek.objects.all().values('id').annotate(Count('nadawca'))
    wyslane = len(wyslane)

    podpisane = Odbiorca.objects.filter(wniosek_status='P')
    podpisane = len(podpisane)

    data = {'wyslane': wyslane, 'podpisane' : podpisane}
    return str_to_json(callback,data)

## -----------------------------------------------------
def petycja_podpisane(request, callback):
    podpisane = Odbiorca.objects.filter(wniosek_status='P').order_by('imie')
    return query_to_json(callback,podpisane)

## -----------------------------------------------------
def petycja_podpisane_druga_tura(request, callback):
    podpisane = Odbiorca.objects.filter(wniosek_status='P').filter(druga_tura=True).order_by('imie')
    return query_to_json(callback,podpisane)

## -----------------------------------------------------
def wniosek_wyslij(request):
    
    if request.method != 'POST' :
        return HttpResponse('Only POST accepted')

    data = request.POST.keys()[0]
    data = json.loads(data)

    recievers   = Odbiorca.objects.filter(id__in=data['odbiorcy'])
    subject     = Config.wniosek['email']['subject']
    emailFrom   = data['email']
    emailFrom   = Config.wniosek['email']['reply']
    sent_ok     = True
    skipped     = False
    sender      = {'imie':data['imie'], 'nazwisko':data['nazwisko'], 'adres':data['adres'], 'email':data['email']}
    save_sender = data['zachowaj']
    sent_ok_count     = 0
    sent_error_count  = 0
    
    #------------------
    # Save sender
    if save_sender :
        try: 
            nadawca = Nadawca.objects.create(imie=data['imie'], nazwisko=data['nazwisko'], email=data['email'], adres=data['adres'])
        except:
            # User with this email already exists
            pass
    else :
        nadawca = Nadawca.objects.filter(id=-1)
        
        if len(nadawca) == 0 :
            try : 
                nadawca = Nadawca.objects.create(id=-1, imie='ukryty', nazwisko='ukryty', email='ukryty', adres='ukryty')
            except :
                pass
        else:
            nadawca = nadawca[0]
 
    #--------------
    # Send petition to 
    # every reciever
    for p in recievers :
        reciever    = {'imie':p.imie, 'nazwisko':p.nazwisko, 'plec':p.plec }
        petycja     = get_petycja_text(reciever, sender)
        message     = petycja
         
        try:
            skipped = False
            email = EmailMessage(subject, message, emailFrom,
                                 [p.email], [Config.wniosek['email']['copy']] )
              
#                                  headers = {'Reply-To': Config.wniosek['email']['reply']})
            email.send()
   
        except:
            sent_ok = False
            skipped = True
   
#          
        if skipped :
            sent_error_count = sent_error_count + 1
  
        else :
            sent_ok_count = sent_ok_count + 1
            Odbiorca.objects.filter(id=p.id).update(wniosek_status='W')
    
            try:
                Wniosek.objects.create(nadawca=nadawca, odbiorca=p)
            except Exception as e:
                print "Problem z wysylaniem [start]"
                print e
                print "Problem z wysylaniem [end]"
    

#     return HttpResponse()

    ##--------------------
    # Send confirmation
    try:
        subject = "Potwierdzenie wyslania"
        message = 'Wyslane petycje: \n- poprawnie : ' + str(sent_ok_count) + "\n- problem :" + str(sent_error_count) + "\n"
        emailFrom = "automat@gmail.com"
        emailTo = Config.wniosek['email']['reply']
        email = EmailMessage(subject, message, emailFrom,  [emailTo] )

        email.send()
    except:
        pass
    
    return HttpResponse()
       
## -----------------------------------------------------     
def test(request):
#     email = EmailMessage(
#                          'Hello', 
#                          'Body goes here', 
#                          'from@example.com',
#                          ['przemek@blackted.com', 'to2@example.com'], 
#                          ['purbanski@interia.pl'],
#             headers = {'Reply-To': 'another@example.com'})
#     email.send()
    
    return HttpResponse('test:' )




