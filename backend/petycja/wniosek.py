# -*- coding: utf-8 -*-

import re
import pprint
import sys

def get_petycja_text(reciever, sender):
    head = ''
    body = ''
    bottom = ''
    
    reload(sys)
    sys.setdefaultencoding('utf8')

    if reciever['plec'] == 'M' :
        head = u'''Szanowny Panie,
        
Cieszę się, że zdecydował się Pan kandydować na stanowisko Prezydenta Miasta.
 
Zastanawiam się, na którą z kandydujących osób mam zagłosować. Chcę, żeby ta, która zostanie wybrana na urząd prezydenta, była otwarta na mieszkańców – na ich pytania, oczekiwania i potrzeby. Kiedy będę dokonywać wyboru, zwrócę uwagę na to, jakie są plany kandydatów dotyczące jawności wydawania publicznych pieniędzy oraz czy wyrażają gotowość do informowania, w jaki sposób realizują swoje zadania. Ponadto zależy mi na wyraźnej deklaracji dotyczącej niekoncentrowana pełni władzy w rękach jednej osoby. 
        
Dlatego zwracam się do Pana z prośbą o zadeklarowanie, czy w razie wygrania wyborów na Prezydenta Miasta, wprowadzi Pan wskazane poniżej rozwiązania, które będą realną gwarancją realizacji moich oczekiwań:

1.  Jawne, dostępne w Biuletynie Informacji Publicznej, rejestry wszystkich umów zawieranych przez urząd. Rejestry powinny być dostępne w formacie możliwym do dalszego wykorzystywania oraz zawierać minimalnie informację o numerze umowy, kontrahencie, zakresie wykonywanego zadania, terminie obowiązywania umowy oraz kwocie.

2.  Kalendarz dostępny w Biuletynie Informacji Publicznej, w formacie możliwym do dalszego wykorzystywania, zawierający wszystkie przeszłe, bieżące i planowane służbowe spotkania oraz wszelkie działania związane z wykonywaniem przez Pana funkcji publicznej. 
        
3.  Obietnica niezatrudniania radnych w spółkach i instytucjach podległych prezydentowi miasta.

Takie zobowiązania zbierane są przez platformę wybory.siecobywatelska.pl. Wystarczy, że wyśle Pan informację na adres: wybory@wiadomosci.siecobywatelska.pl wskazując wyraźnie, że podpisuje się Pan pod wszystkimi zobowiązaniami. 

Proszę o wprowadzenie tych rozwiązań od samego początku kadencji. Jeśli to Pan wygra wybory, co roku w Dniu Samorządu Terytorialnego (27 maja), włączę się w akcję sprawdzania jak wywiązała się Pan z realizacji tej obietnicy.

Łączę wyrazy szacunku i – jeśli obieca Pan i konsekwentnie będzie realizował jawność – wygranej!
'''
    else :
        head = u'''Szanowna Pani,
        
Cieszę się, że zdecydowała się Pani kandydować na stanowisko Prezydenta Miasta.
 
Zastanawiam się, na którą z kandydujących osób mam zagłosować. Chcę, żeby ta, która zostanie wybrana na urząd prezydenta, była otwarta na mieszkańców – na ich pytania, oczekiwania i potrzeby. Kiedy będę dokonywać wyboru, zwrócę uwagę na to, jakie są plany kandydatów dotyczące jawności wydawania publicznych pieniędzy oraz czy wyrażają gotowość do informowania, w jaki sposób realizują swoje zadania. Ponadto zależy mi na wyraźnej deklaracji dotyczącej niekoncentrowana pełni władzy w rękach jednej osoby.
 
Dlatego zwracam się do Pani z prośbą o zadeklarowanie, czy w razie wygrania wyborów na Prezydenta Miasta, wprowadzi Pani wskazane poniżej rozwiązania, które będą realną gwarancją realizacji moich oczekiwań:

1.  Jawne, dostępne w Biuletynie Informacji Publicznej, rejestry wszystkich umów zawieranych przez urząd. Rejestry powinny być dostępne w formacie możliwym do dalszego wykorzystywania oraz zawierać minimalnie informację o numerze umowy, kontrahencie, zakresie wykonywanego zadania, terminie obowiązywania umowy oraz kwocie.

2.  Kalendarz dostępny w Biuletynie Informacji Publicznej, w formacie możliwym do dalszego wykorzystywania, zawierający wszystkie przeszłe, bieżące i planowane służbowe spotkania oraz wszelkie działania związane z wykonywaniem przez Panią funkcji publicznej. 

3.  Obietnica niezatrudniania radnych w spółkach i instytucjach podległych prezydentowi miasta.

Takie zobowiązania zbierane są przez platformę wybory.siecobywatelska.pl. Wystarczy, że wyśle Pani informację na adres: wybory@wiadomosci.siecobywatelska.pl wskazując wyraźnie, że podpisuje się Pani pod wszystkimi zobowiązaniami. 

Proszę o wprowadzenie tych rozwiązań od samego początku kadencji. Jeśli to Pani wygra wybory, co roku w Dniu Samorządu Terytorialnego (27 maja), włączę się w akcję sprawdzania jak wywiązała się Pani z realizacji tej obietnicy.

Łączę wyrazy szacunku i – jeśli obieca Pani i konsekwentnie będzie realizowała jawność – wygranej!
'''

#     bottom = '''
# [NADAWCA_IMIE] [NADAWCA_NAZWISKO]
# [NADAWCA_MIEJSCE_ZAMIESZKANIA]
# '''

    bottom = u'''
[NADAWCA_IMIE] [NADAWCA_NAZWISKO]

'''
        
    body = '''
        Janowść jest fajna
    '''
    txt = head  +  bottom
        
#     nazwisko = u' '.str(reciever['nazwisko']).encode('utf-8').strip()
#     ret = re.sub(r'\[KANDYDATA_NAZWISKO\]', nazwisko, txt)
    ret = re.sub(ur'\[KANDYDATA_NAZWISKO\]', str(reciever['nazwisko']), txt)
    ret = re.sub(ur'\[NADAWCA_NAZWISKO\]', str(sender['nazwisko']), ret)
    ret = re.sub(ur'\[NADAWCA_IMIE\]', str(sender['imie']), ret)
    ret = re.sub(ur'\[NADAWCA_MIEJSCE_ZAMIESZKANIA\]', str(sender['adres']), ret)
    return ret 
