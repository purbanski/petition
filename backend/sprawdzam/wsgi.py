"""
For more information on this file, see
https://docs.djangoproject.com/en/1.6/howto/deployment/wsgi/
"""

import os
import sys

sys.path.append('/home/emil/workspace/sprawdzam/backend')

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sprawdzam.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
