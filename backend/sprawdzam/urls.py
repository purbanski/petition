from django.conf.urls import patterns, include, url
from petycja import views

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^odbiorcy/callback=([a-zA-Z0-9-_.]{1,})$',    views.odbiorcy),
    url(r'^odbiorcy-druga-tura/callback=([a-zA-Z0-9-_.]{1,})$',    views.odbiorcy_druga_tura),
    url(r'^organizacje/callback=([a-zA-Z0-9-_.]{1,})$', views.organizacje),
    url(r'^wojewodztwa/callback=([a-zA-Z0-9-_.]{1,})$', views.wojewodztwa),
    url(r'^miasta/callback=([a-zA-Z0-9-_.]{1,})$',      views.miasta),
    url(r'^petycja-stats/callback=([a-zA-Z0-9-_.]{1,})$',views.petycja_stats),
    url(r'^petycja-podpisane-druga-tura/callback=([a-zA-Z0-9-_.]{1,})$',views.petycja_podpisane_druga_tura),
    url(r'^petycja-podpisane/callback=([a-zA-Z0-9-_.]{1,})$',views.petycja_podpisane),
    url(r'^wniosek/wyslij$',                            views.wniosek_wyslij),
#     url(r'^test$', views.test),
)
