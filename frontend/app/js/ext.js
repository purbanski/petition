//---------------------------------------------------------
// Array
//---------------------------------------------------------
Array.prototype.contains = function(obj) {
    var i = this.length;
    while (i--) {
        if (this[i] == obj) {
            return true;
        }
    }
    return false;
}
//---------------------------------------------------------
Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
}
//---------------------------------------------------------
Array.prototype.dump = function() {
	console.log('length: ' + this.length);
	console.log('--');
	for (var i = 0; i < this.length; i++) {
	    console.log( i + ':' + this[i]);
	}
}


//---------------------------------------------------------
// Validate Email
//---------------------------------------------------------
function validateEmail(email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
} 