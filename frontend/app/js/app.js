'use strict';

// Declare app level module which depends on filters, and services
angular.module('myApp', [
  'ngRoute',
  'myApp.filters',
  'myApp.services',
  'myApp.directives',
  'myApp.controllers'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/problem', 		{templateUrl: 'partials/problem/main.html', controller: 'CtrlHome'});
  $routeProvider.when('/rozwiazanie',	{templateUrl: 'partials/petycja/main.html', controller: 'PetycjaCtrl'});
  $routeProvider.when('/petycja', 		{templateUrl: 'partials/petycja/main.html', controller: 'PetycjaCtrl'});
  $routeProvider.when('/onas', 			{templateUrl: 'partials/onas/onas.html', controller: 'CtrlHome'});
  $routeProvider.when('/media', 		{templateUrl: 'partials/media/media.html', controller: 'CtrlHome'});
  $routeProvider.when('/kandydat', 		{templateUrl: 'partials/kandydat/kandydat.html', controller: 'CtrlHome'});
  $routeProvider.when('/kontakt', 		{templateUrl: 'partials/kontakt/kontakt.html', controller: 'CtrlHome'});
  $routeProvider.when('/home', 			{templateUrl: 'partials/home/main.html', controller: 'CtrlHome'});

//  $routeProvider.when('/test', 		{templateUrl: 'test.html', controller: 'CtrlHome'});
//  $routeProvider.when('/view2', 		{templateUrl: 'partials/partial2.html', controller: 'MyCtrl2'});
  $routeProvider.otherwise({redirectTo: '/home'});
}]);
