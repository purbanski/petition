'use strict';

/* Filters */

angular.module('myApp.filters', []).
  filter('interpolate', ['version', function(version) {
    return function(text) {
      return String(text).replace(/\%VERSION\%/mg, version);
    };
  }]);

 
 angular.module('myApp')
.filter('to_trusted', ['$sce', function($sce){
        return function(text) {
		var zeroS = '0';
		var temp = text.toString();
		if(temp.length<2){for(var i=0;i<4;i++){temp = zeroS.concat(temp);}}
		if(temp.length<3){for(var i=0;i<3;i++){temp = zeroS.concat(temp);}}
		if(temp.length<4){for(var i=0;i<2;i++){temp = zeroS.concat(temp);}}
		if(temp.length<5){temp = zeroS.concat(temp);}
         var elo = '';
		 
            for(var i =0; i<temp.length;i++){
            elo += '<span>'+temp.charAt(i)+'</span>';
            }
			
            return $sce.trustAsHtml(elo);
        };
    }]);