'use strict';

/* Controllers */

angular.module('myApp.controllers')
  .controller('CtrlHome', ['$scope', '$sce', '$http', '$routeParams', 'configuration', 'texts', function($scope, $sce, $http, $routeParams, config, texts ){
	
	  //adi
	  $scope.check_credentials = function () {
		document.getElementById("ajax").textContent = "";

		var request = $http({
		    method: "post",
		    url: window.location.href.split("#/")[0] + "send_zgloszenie.php",
		    data: {
				name: $scope.name,
		        email: $scope.mail,
		        message: $scope.msg
				
		    },
		    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		});
		
		/* Check whether the HTTP Request is Successfull or not. */
		request.success(function (data) {
		    document.getElementById("ajax").textContent = data;
			if(document.getElementById("ajax").innerHTML[0]=="D"){document.getElementById("ajax").style.color="#5ED622";setTimeout(function(){openContact();document.getElementById("ajax").textContent = "";}, 2500);}else{document.getElementById("ajax").style.color="#F22C00";}
		});
	}
	  
	  
	  $scope.$on('$viewContentLoaded', function() {thatPage();});//adi
	  $scope.licznik = {};
	  $scope.licznik.petycje = {};
	  $scope.licznik.petycje.podpisane 	= 0;
	  $scope.licznik.petycje.wyslane 	= 0;

	  $scope.petycja = {}
	  
	  $scope.updateLiczniki = function() {
		  var url = config.apiUrl + 'petycja-stats/callback=JSON_CALLBACK';
		  $http({method:'JSONP', url: url}).
		  	success(function(data) {
		  		$scope.licznik.petycje = data.data[0];
		  	});  
	  }
	  
	  //---------------------------------------
	  // pobiera organizacje
//	  var url = config.apiUrl + 'petycja-podpisane/callback=JSON_CALLBACK'; 
	  var url = config.apiUrl + 'petycja-podpisane-druga-tura/callback=JSON_CALLBACK'; 
	  $http({method:'JSONP', url: url}).
	  	success(function(data) {
	  		$scope.petycja.podpisane = data.data;
///	  		$('.dropdown-menu').on('click', function(e) {
//	  			e.stopPropagation(); 
//	  		});
	  	});  

	  $scope.updateLiczniki();
  }]);

