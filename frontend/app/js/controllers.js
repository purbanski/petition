'use strict';

/* Controllers */
angular.module('myApp.controllers', [])
  .controller('PetycjaCtrl', ['$scope', '$sce', '$http', '$routeParams', 'configuration', 'texts', function($scope, $sce, $http, $routeParams, config, texts ){
	  
	  //---------------------------------------
	  
	  $scope.$on('$viewContentLoaded', function() {thatPage();});
	  $scope.odbiorcy = {};
	  $scope.odbiorcy.wszyscy = [];
	  $scope.odbiorcy.wybrani = [];
	  $scope.odbiorcy.search = {};
	  $scope.odbiorcy.search.wojewodztwo = '';
	  
	  $scope.texts = texts;
	  
	  $scope.licznik = {};
	  $scope.licznik.petycje = {};
	  $scope.licznik.petycje.podpisane 	= 0;
	  $scope.licznik.petycje.wyslane 	= 0;
	  //adi
	  $scope.my_htmlP = '';
	  $scope.my_htmlW = '';
	  //adi
	  //---------------------------------------
	  $scope.wniosek = {};
	  $scope.wniosek.shown = false;
	 
	  
	  $scope.wniosek.toggle = function() {
		  $scope.wniosek.shown = !$scope.wniosek.shown;
		  $scope.updateButtons();
	  }

	  $scope.search = {};
	  $scope.search.organizacje = [];
	  
	  $scope.show_loader = true;
	  
	  ///adi
	  $scope.search.shown = false;
	 
	  
	  $scope.search.toggle = function() {
		  $scope.search.shown = !$scope.search.shown;
		  //$scope.updateButtons();
	  }
	  /////adi
	  //---------------------------------------
	  $scope.updateButtons = function() {
		  if ($scope.wniosek.shown)
			  $scope.wniosek.button = 'Schowaj wniosek';
		  else
			  $scope.wniosek.button = 'Podgl\u0105d petycji';
		  
	  }
	  
	  //---------------------------------------
	  $scope.updateLiczniki = function() {
		  var url = config.apiUrl + 'petycja-stats/callback=JSON_CALLBACK'; 
		  $http({method:'JSONP', url: url}).
		  	success(function(data) {
		  		$scope.licznik.petycje = data.data[0];
		  	});  
	  }

	  //---------------------------------------
	  $scope.initSaveSenderCheckbox = function() {
	  }
	  
	  //---------------------------------------
	  $scope.toggleSaveSender = function() {
		  var eid = 'save-sender-checkbox';
		  var e = document.getElementById( eid );
		  e.checked = !e.checked;  
	  }

	  
	  //---------------------------------------
	  $scope.searchToggleWojewodztwo = function(id, nazwa) {
		  var eid = 'select-woj-' + id;
		  var e = document.getElementById( eid );
		  
		  if (e.checked)
			  $scope.odbiorcy.search.wojewodztwo = nazwa;
		  else
			  $scope.odbiorcy.search.wojewodztwo = '';
	  }

//	  $scope.searchOrgToggle = function( org ) {
//		  if ( $scope.search.organizacje.contains(org) )
//		  {	  
//			  $scope.search.organizacje.remove(org);
//		  }
//		   else
//		  {
//			   $scope.search.organizacje.push(org); 
//		  }
//		  $scope.updateReciepientsCheckbox();
//	  }
	  

	  
	  //---------------------------------------
	  $scope.togglePerson = function( id ) {
		if ( $scope.odbiorcy.wszyscy[id-1] != null && $scope.odbiorcy.wszyscy[id-1].wniosek_status == 'P' )
				return;

		var eid = 'czlowiek-checkbox-' + id;
		var e = document.getElementById( eid );
		e.checked = !e.checked;

		$scope.setReciepient( id, e.checked );
	  }

	  //---------------------------------------
	  $scope.togglePersonOrg = function(id) {
		  var eid = 'czlowiek-checkbox-' + id;
		  var e = document.getElementById( eid );
		  $scope.setReciepient( id, e.checked );
		  
//			console.log($scope.odbiorcy.wszyscy[id]);

	  }
	  
	  //---------------------------------------
	  $scope.setReciepient = function( id, check ) {
		  if ( check && ! $scope.odbiorcy.wybrani.contains(id) )
			  $scope.odbiorcy.wybrani.push(id);
	  
		  if ( !check && $scope.odbiorcy.wybrani.contains(id) )
			  $scope.odbiorcy.wybrani = $scope.odbiorcy.wybrani.remove(id);
	  }
	//---------------------------------------
	  $scope.updateReciepientsCheckbox = function() {
		  var odbiorcy = $scope.ludzie;
//		  console.log('called' + $scope.odbiorcy.length);
		  $scope.odbiorcy.dump();
		  angular.forEach(odbiorcy, function(value, key) {
			  var eid = 'czlowiek-checkbox-' + value.id;
			  var e = document.getElementById( eid );
			  if (e )
			  {
//				  console.log(value.id);
				  if ( $scope.odbiorcy.contains(value.id) )
					  e.checked = true;
				  else 
					  e.checked = false;
			  }
		  });	  
	  }
	  
	  //---------------------------------------
	  $scope.ludzieFilter = function( value ) {
		  if ( $scope.search.organizacje.length == 0 )
			  return true;
		  
		  return $scope.search.organizacje.contains(value.organizacja);
	  }

	  //---------------------------------------
	  $scope.toggleAllPeople = function() {
		  var people = $scope.ludzie;
		  var tid 	= document.getElementById( 'toggle-all-people');
		  var check = tid.checked;
		  
		  angular.forEach(people, function(value, key){
			  var eid = 'czlowiek-checkbox-' + value.id;
			  var e = document.getElementById( eid );
			  if (e)
			  {
				  e.checked = check;
				  $scope.setReciepient( value.id, check )
			  }
//			  console.log(value );
		  });
	  }
	  
	  //---------------------------------------
	  var getWnCss = function( array, len ) {
		  var ret = {};
		  
		  if ( typeof(len) == 'undefined')
			  len = 3;
		  
		  if ( array.length < len )
		  {
			  ret['ret'] = false;
			  ret['css'] = "wn-error";
			  return ret;
		  }
			
		  ret['ret'] = true;
		  ret['css'] = "wn-ok";
		  return ret;
	  }

	  //---------------------------------------
	  var getWnCssEmail = function() {
		  var ret = {};
		  if ( ! validateEmail($scope.form.email) )
		  {
			  ret['ret'] = false;
			  ret['css'] = "wn-error";
			  return ret;

		  }
		  ret['ret'] = true;
		  ret['css'] = "wn-ok";

		  return ret;
	  }
	  //---------------------------------------
	  $scope.form = {};
	  $scope.form.imie 		= "";
	  $scope.form.nazwisko 	= "";
	  $scope.form.email		= "";
	  $scope.form.adres		= "";
	  
	  $scope.sendBtnEnable = true;
	  
	  $scope.setFormImie 	= function(imie){ $scope.form.imie = imie; }
	  $scope.setFormNazwisko= function(str) { $scope.form.nazwisko = str; }
	  $scope.setFormEmail 	= function(str)	{ $scope.form.email = str; }
	  $scope.setFormAdres 	= function(str)	{ $scope.form.adres = str;	}
	  
	  //---------------------------------------
	  $scope.wnImieCss 		= function() { return getWnCss( $scope.form.imie )['css'];  }
	  $scope.wnNazwiskoCss 	= function() { return getWnCss( $scope.form.nazwisko )['css'];  }
	  $scope.wnAdresCss 	= function() { return getWnCss( $scope.form.adres, 12 )['css']; }
	  $scope.wnEmailCss 	= function() { return getWnCssEmail()['css']; }

	  $scope.sendWniosekEnable = function() {
		  if ( 
				  $scope.form.imie.length > 2 &&
				  $scope.form.nazwisko.length > 2 &&
				  $scope.form.email.length > 7 &&
				  $scope.form.adres.length > 3
				  )
			  return true;

		  return false;
		  
//		  if ( getWnCss( $scope.form.imie )['ret'] && 
//				  getWnCss( $scope.form.nazwisko )['ret'] && 
//				  getWnCss( $scope.form.adres,12 )['ret'] && 
//				  getWnCssEmail()['ret'] && 
//				  $scope.odbiorcy.wybrani.length &&
//				  $scope.sendBtnEnable )
//			  return true;
//		  return false;
	  }
	  //---------------------------------------
	  $scope.sendWniosek = function() {
		  var sender_data = {
			'imie' 		: $scope.form.imie,
			'nazwisko' 	: $scope.form.nazwisko,
			'adres'		: $scope.form.adres,
			'email'		: $scope.form.email,
			'odbiorcy'	: $scope.odbiorcy.wybrani,
			'zachowaj'	: document.getElementById( 'save-sender-checkbox' ).checked
		  };


//		  console.log(sender_data);
		  var url = config.apiUrl + 'wniosek/wyslij';
		  $scope.sendBtnEnable = false;

		  $http({
			  method:'POST', 
			  url: url, 
			  data: sender_data,
			  headers: {'Content-Type': 'application/x-www-form-urlencoded'} 
		  }).
		  	success(function(data) {
		  	  $scope.sendBtnEnable = true;

		  		$scope.updateLiczniki();
		  		console.log(sender_data);
		  		alert("Ilosc wyslanych petycji: " + sender_data.odbiorcy.length );
		  		console.log(data+" get complete: "+url);
		  	}).
		  	error(function(data) {
		  	  $scope.sendBtnEnable = true;

//		  		$scope.updateLiczniki();
//		  		alert("wyslalismy");
//		  		console.log(data+" get complete: "+url);
		  	});
	  }

	  $scope.getOdbiorcaFacebookLink = function(link) {
		  if ( typeof link == 'undefined' || link == 'BRAK' )
			  return 'BRAK';
		  
		  return link;
	  }

	  $scope.getOdbiorcaFacebookImg = function(link) {
		  if ( typeof link == 'undefined' || link == 'BRAK' )
			  return 'empty_btn.png';
		  
		  return 'facebook_btn.png';
	  }

	  $scope.getOdbiorcaTwitterLink = function(link) {
//		  console.log(link);
		  if ( typeof link == 'undefined' || link == 'BRAK' )
			  return 'BRAK';
		  
		  return link;
	  }
	  
	  $scope.getOdbiocaFacebookHide = function(link) {
		  return false;
		  if ( typeof link == 'undefined' || link == 'BRAK' )
			  return true;
		  return false;
	  }

	  $scope.getOdbiocaTwitterHide = function(link) {
		  if ( typeof link == 'undefined' || link == 'BRAK' )
			  return true;
		  return false;
	  }

	  $scope.getOdbiorcaTwitterImg = function(link) {
		  if ( typeof link == 'undefined' || link == 'BRAK' )
			  return 'twitter_btn_dis.png';
		  
		  return 'twitter_btn.png';
	  }

	  $scope.getOdbiorcaDivClass = function(status) {
//		  console.log(status);
		  switch (status) 
			{
			  case 'W' : return 'alert-info';
			  case 'P' : return 'alert-success';
			  case 'O' : return 'alert-danger';
				  
			default:
				return 'alert-warning';
			  
			}
	  }
	  
	  $scope.getOdbiorcaStatus = function(status) {
		  switch (status) 
			{
			  case 'W' : return 'Wyslany';
			  case 'P' : return 'Podpisany';
			  case 'O' : return 'Odrzucony';
				  
			default:
				return 'Pusty';
			  
			}
		  
		  if (status=='W') return "Wyslany";
	  }
	  

	  $scope.getOdbiorcaCheckboxStatus = function(status) {
		  if ( status == 'P' )
			  return true;
		  return false;
	  }
	  

	  //---------------------------------------
	  $scope.updateButtons();

	  //---------------------------------------
//	  var url = config.apiUrl + 'odbiorcy/callback=JSON_CALLBACK'; 
	  var url = config.apiUrl + 'odbiorcy-druga-tura/callback=JSON_CALLBACK'; 
	  $http({method:'JSONP', url: url}).
	  	success(function(data) {
	  		$scope.show_loader = false;
	  		$scope.odbiorcy.wszyscy = data.data;
	  	}).
	  	error(function(data, status, headers, config) {
	  		$scope.show_loader = false;
	  	});	
	
	  //---------------------------------------
	  // pobiera organizacje
	  var url = config.apiUrl + 'organizacje/callback=JSON_CALLBACK'; 
	  $http({method:'JSONP', url: url}).
	  	success(function(data) {
	  		$scope.organizacje = data.data;
	  		$('.dropdown-menu').on('click', function(e) {
	  			e.stopPropagation(); 
	  		});
	  	});  

	  //---------------------------------------
	  // pobiera statystyki
	  $scope.updateLiczniki();
		  
	  //---------------------------------------
	  // pobiera miasta
	  var url = config.apiUrl + 'miasta/callback=JSON_CALLBACK'; 
	  $http({method:'JSONP', url: url}).
	  	success(function(data) {
	  		$scope.miasta = data.data;
	  		$('.dropdown-menu').on('click', function(e) {
	  			e.stopPropagation(); 
	  		});
	  	}); 
	  
	  //---------------------------------------
	  // pobiera wojewodztwa
	  var url = config.apiUrl + 'wojewodztwa/callback=JSON_CALLBACK'; 
	  $http({method:'JSONP', url: url}).
	  	success(function(data) {
	  		$scope.wojewodztwa = data.data;
	  		$('.dropdown-menu').on('click', function(e) {
	  			e.stopPropagation(); 
	  		});
	  	}); 

  }])
  .controller('MyCtrl2', ['$scope', function($scope) {
		
  }]);

