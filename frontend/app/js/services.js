'use strict';

/* Services */
angular.module('myApp.services', []).
  value('version', '0.2').
  constant('configuration', {
	  apiUrl 			: '/petycja-api/',
	  dane_osobe_info 	: 'blah' 
  }).
  constant('texts', {
	  apiUrl 			: '/petycja-api/',
	  dane_osobe_info	: 'Wyra\u017cam zgod\u0119 na przesy\u0142anie informacji o dzia\u0142alno\u015bci programowej i przetwarzanie moich danych osobowych przez znajduj\u0105c\u0105 si\u0119 przy ul. Ursynowskiej 22/2 w Warszawie Sie\u0107 Obywatelsk\u0105 - Watchdog Polska, zgodnie z Regulaminem Ochrony Danych Osobowych' 

  });
